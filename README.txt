/* $Id$ */

-- SUMMARY --

The predelete module hooks into the deletion process of nodes. By default it is
not possible to react on a deleteion attempt before the deletion of a node. This
is cured by providing the hook_predelete_node(). Other modules may implement the
hook and add custom checks on the node that is about to be deleted.

The module ships with an API documentation and an example module that provides
a single checkbox field. Nodes that contain the field could only be deleted if
the checkbox is checked.

To Do:
A screencast and a blogpost about this module are coming soon.


For a full description of the module, visit the project page:
  http://drupal.org/project/predelete

To submit bug reports and feature suggestions, or to track changes:
  http://drupal.org/project/issues/predelete


-- REQUIREMENTS --

None.

-- INSTALLATION --

* Install as usual, see http://drupal.org/node/70151 for further information.

* Implement the hook_predelete_node() as described in this README or enable the
  predelete_field module.


-- CONFIGURATION --

* Configure user permissions in Administration » People » Permissions »
  predelete module:

  - bypass predeletion check

    Users in roles with this permission are allowed to delete nodes directly
    without the function of this module. By default the deletion attempt od all
    users are validated by this module (even those of uid==1).


-- CUSTOMIZATION --

* Implement the hook_predelete_node as described in predelete.api.php


-- CONTACT --

Current maintainers:
* Mirko Haaser (McGo) - http://drupal.org/user/87891
